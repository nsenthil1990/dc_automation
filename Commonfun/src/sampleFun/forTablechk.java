package sampleFun;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class forTablechk {
	public static void main(String[] args){
		WebDriver driver=null;
		driver=new FirefoxDriver();
		driver.get("http://www.toolsqa.com/automation-practice-table/#");
		WebElement table=driver.findElement(By.className("tsc_table_s13"));
		List<WebElement> rows=table.findElements(By.tagName("tr"));
		//System.out.println(rows.size());
		for(WebElement col:rows){
			List<WebElement> column=col.findElements(By.tagName("td"));
		     //System.out.println(column.size());
		    for(WebElement cell:column){
		    	System.out.println(cell.getText());
		    	
		    }
		    List<WebElement> th=col.findElements(By.tagName("th"));
		    for(WebElement inner:th){
		    	System.out.println(inner.getText());
		    }
		    System.out.println("----------");
		}
	driver.quit();	
		
	}

}
