package sampleFun;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class commonMethods {
public static WebElement element=null;

//for link click event
public static WebElement linkClick(WebDriver driver){
	element=driver.findElement(By.id(""));
	return element;
}

// for dropdownselection for value
public static void drpdownSelection(WebDriver driver,String dom,String value){
	if(dom=="ID"){
	Select drpdownsel=new Select(driver.findElement(By.id(dom)));
	drpdownsel.selectByValue(value);
	}
}

//for button click event
public static void clickEve(WebDriver driver,String dom){
	driver.findElement(By.id("dom")).click();
}

//for text enter event
public static void txtEnter(WebDriver driver,String dom,String txt){
	driver.findElement(By.id("dom")).sendKeys(txt);
}

//for maxmizing the browser window
public static void windowMaximize(WebDriver driver){
	driver.manage().window().maximize();
}

public static void implicWait(WebDriver driver){
	driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
}

}
