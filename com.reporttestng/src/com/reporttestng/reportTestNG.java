package com.reporttestng;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class reportTestNG {
	public static WebDriver driver;
	@BeforeTest
	public void start(){
	    driver=new FirefoxDriver();
		driver.manage().window().maximize();
		//driver.get("http://www.guru99.com/pdf-emails-and-screenshot-of-test-reports-in-selenium.html");
	}
  @Test
  public void f() {
	  driver.navigate().to("http://www.guru99.com/pdf-emails-and-screenshot-of-test-reports-in-selenium.html");
		 ((JavascriptExecutor) driver)
      .executeScript("window.scrollTo(0, document.body.scrollHeight)");
  }
  
  @AfterTest
  public void stop(){
	  driver.quit();
  }
}
