package Practice;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class baseClass {
 public WebDriver driver=new FirefoxDriver();
 
@BeforeTest
public void beforeTest(){
	driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	driver.manage().window().maximize();
	driver.get("http://only-testing-blog.blogspot.in/2014/05/login.html");	
}
@AfterTest
public void quit(){
	driver.quit();
}
@DataProvider
public Object[][] credential(){
	Object cred[][]=new Object[2][2];
	cred[0][0]="user1";
	cred[0][1]="pass1";
	cred[1][0]="user2";
	cred[1][1]="pass2";
	
	return cred;
	
}
@Test(dataProvider="credential")
public void starttest(String username,String password){
	  driver.findElement(By.xpath("//input[@name='userid']")).clear();
	   driver.findElement(By.xpath("//input[@name='pswrd']")).clear();
	   driver.findElement(By.xpath("//input[@name='userid']")).sendKeys(username);
	   driver.findElement(By.xpath("//input[@name='pswrd']")).sendKeys(password);
	   driver.findElement(By.xpath("//input[@value='Login']")).click();
	   String alrt = driver.switchTo().alert().getText();
	   driver.switchTo().alert().accept();
	   System.out.println(alrt);
}
}
